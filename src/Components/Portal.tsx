import React, { useEffect, useState } from 'react'
import ReactDOM from 'react-dom'

//Custom portal without libs

// div id where you open portal
/**
 * @param containerId
 *
 */
interface ICreatePortalProps {
    containerId: string
}
/**
* @param  {ReactNode} children , ICreatePortalProps} containerId
*@return {HTMLElement || null}
*/
const CreatePortal: React.FC<ICreatePortalProps> = ({ children, containerId }) => {
    const [element, setElement] = useState<HTMLElement>()
    useEffect(() => {
        const container = document.getElementById(containerId)
        if (container) {
            setElement(container)
        }
    }, [containerId])

    if (element) {
        return ReactDOM.createPortal(
            children,
            element
        )
    }
    return null
}
export default CreatePortal

// <CreatePortal containerId='modal'>
//     <YouComponent />
//     </CreatePortal>
