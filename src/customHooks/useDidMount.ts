import { useState, useEffect } from 'react'
// Check first render

export const  useDidMount = () => {
    const [hasRendered, setHasRendered] = useState(false)
    useEffect(() => setHasRendered(true), [hasRendered])
    return hasRendered
}

//EXAMPLE

//const hasRendered = useDidMount();

