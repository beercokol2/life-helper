import { isServer } from './isServer'

// check mobile , please use at your own risk.

const isMobile = {
    Android: function() {
        return !isServer && navigator.userAgent.match(/Android/i)
    },
    BlackBerry: function() {
        return !isServer && navigator.userAgent.match(/BlackBerry/i)
    },
    iOS: function() {
        return !isServer && navigator.userAgent.match(/iPhone|iPad|iPod/i)
    },
    Opera: function() {
        return !isServer && navigator.userAgent.match(/Opera Mini/i)
    },
    Windows: function() {
        return (
            !isServer && navigator.userAgent.match(/IEMobile/i) ||
            !isServer && navigator.userAgent.match(/WPDesktop/i)
        )
    },
    any: function() {
        return (
            isMobile.Android() ||
            isMobile.BlackBerry() ||
            isMobile.iOS() ||
            isMobile.Opera() ||
            isMobile.Windows()
        )
    },
}

export default isMobile
